#printing integer
a = 10
print(a)

#printing string
print("Hello World")

#printing list
c = [10, 20, 30]
print(c)

#printing tuple
d = (10, 20)
print(d)

type(10)
type("HelloWorld")
type((10, 20, 30))
type([10, 20, 30])

for i in range(10):
    print(i)

for i in range(10,20):
    print(i)

for i in range(20,40,2):
    print(i)

round(23.5678,2)
round(23.987)
round(23.4892)

'''
input("Enter a Number: ")
input("Enter a Word: ")
var = input("Enter a Number: ")
print(var)
type(var)
var = input("Enter a Word: ")
print(var)
type(var)
'''

var = "Hello World"
len(var)

x = 1
while x < 10:
    print(x)
    x = x + 1

names = ["A", "B", "C", "D"]
for x in names:
    print(x)

str = "123Hello"
str.isalnum()
str = "123@#Hello"
var = str.isalnum()
print(var)

str = "Hello"
str.capitalize()
